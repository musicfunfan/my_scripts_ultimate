#!/usr/bin/env sh

#This is a script to take backups to the external HDD.

intialization ()
{
    echo "I need some info first"
    lsblk
    echo "Give me the path to the backup directory"
    read path
    echo "Give me the path to the directory you want to backup"
    read pathToSave
}

backupFiles()
{
    echo "Do you want to take a Backup of a direcrory or divice ? y/n"
    read choice

    if [ "$choice" == "y" ]
    then
        echo "Is the corect disk connected and mounted ? y/n"
        read choice
    else
        echo "Ok no backup"
    fi

    rsync -avhr --info=progress2 "$pathToSave" "$path" --delete

    echo "Backing up the hdd has been completed"
}

backupPackageList ()
{
    echo "Do you want to backup the installed applications names y/n"
    read choice

    if [ "$choice" == "y" ]
    then
        echo "saving apps names"
        pacman -Q | awk {'print $1'} > "$pathToSave"/stuffToInstall
    else
        echo "backup package list not saved"
    fi
}


backupGitrepos ()
{
    echo "Do you want to backup the local gitrepo ? y/n"
    read choice
    if [ "$choice" == "y" ]
    then
        echo "Saving gitrepos"
        rsync -avhr --info=progress2 /home/"$USER"/gitrepo "$pathToSave"
    else
        echo "backup gitrepo will not be saved"
    fi
}

takeSnaphot()
{
    echo "Do you want to take a snapshot with timeshift y/n"
    read choice
    if [ "$choice" == "y" ]
    then
        echo "taking snapshot default settings"
        echo "root privilages will be needed"
        sudo timeshift --create
    else
        echo "no snapshot taken"
    fi
}

removeSnapshot()
{
    echo "Do you want to save space on the drive by deleting old snapshot? y/n"
    read choice
    if [ "$choice" == "y" ]
    then
        echo "Select the snapshot you want to remove"
        sudo timeshift --delete
    else
        echo "No snapshot will be deleted"
    fi
}

backupCurentScripts()
{
    echo "Do you want to backup the scripts that is now in use ?"
    read choice
    if [ "$choice" == "y" ]
    then
        echo "Saving the scripts"
        rsync -avhr --info=progress2 /home/"$USER"/.config/scripts "$pathToSave"

    else
        echo "scripts will not be saved"
    fi
}

backupFirefoxProfiles()
{
    echo "Do you want to backup firefox data ? y/n"
    read choice
    if [ "$choice" == "y" ]
    then
       echo "Backing up the firefox data"
       rsync -avhr --info=progress2 /home/"$USER"/.mozilla/firefox "$pathToSave"
    else
        echo "We will not save firefox data"
    fi
}

backupPasswords()
{
    echo "Do you want to backup the passwords y/n"
    read choice
    if [ "$choice" == "y" ]
    then
        echo "Backing up the passwords"
        rsync -avhr --info=progress2 /home/"$USER"/.password-store "$pathToSave"
    else
        echo "no passwords will be backup"
    fi
}

backupDotConfig()
{
    echo "Do you want to backup the /home/$USER/.config folder ? y/n"
    read choice
    if [ "$choice" == "y" ]
    then
        echo "Backing up the /home/".$USER"/config folder"
        rsync -avhr --info=progress2 /home/"$USER"/.config "$pathToSave"
    else
        echo "/home/$USER/.config will not be backup"
    fi
}


exitScript()
{
    exit 0
}


intialization
backupPackageList
backupGitrepos
takeSnaphot
removeSnapshot
backupCurentScripts
backupFirefoxProfiles
backupPasswords
backupDotConfig
backupFiles
exitScript
