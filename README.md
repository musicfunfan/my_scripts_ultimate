# What do we have here ?

Here we have my scripts that i use with my system 

# This is all the scripts i have in this repository 

* gpu stats 
    * gpu_stats.sh
* language
    * language.sh
    * show_layout.sh
* media
    * media.sh
    * mediaNotification.sh
    * youtube_dmenu.sh
* power
    * power.sh
* screenshots
    * screenshots.sh
* sites
    * sites.sh
* volumeMeter
    * VolumeMeter.sh
* wallpaper
  * wallpaperEngine.sh
  * SelecteWallpeper.sh
* mount 
  * mount_stuff.sh
* scriptUpdater
  * updateInScript.sh
* Notifications
  * mediaNotifications
  * notificationCenter.sh
* backup
 * Backup.sh

# What can i do with that ?

You can use them or take ideas from them or tell me what i do wrong.
I am not a pro in scripting i am more of a noob.

# Why is this here ?

This is here for my backup and also to share it with my friends and the world.

# gpu stats script

This is a script to show the gpu usage and the gpu temperature 
is working only for AMD gpus.

In order for this script to work you need to use the amdgpu open-source driver and you need to have radeontop installed also you need to have radeontop running with this command ``radeontop -d gpu.txt -l 0 > /dev/null &``
This is command will out put the gpu metrics and data to the gpu.txt file and then the script will read that and will print it.

This script has been used in my Polybar config to show gpu status.

# The language scripts 

The script with the name language.sh sets up the the double layout keyboard i use for Greek and English. Also set up the keys i need to press in order change layout.

The script with the name show_layout.sh. Just prints out the layout. But if you using the Polybar this script does not needed this is only for i3status

# Media scripts 

The media scripts have a main script, this script is the media.sh. When you run this you get a dmenu prompt and you need to select a mode (YouTube, videos, photos ,music). When you select a mode the dmenu script will print all the stuff in the directory that you have specify (in the code of the script) and will print it in a dmenu prompt so you can select what you want to play. When you do that the media file will start playing with mpv media player.

If you select the YouTube option, that will run the script youtube_dmenu.sh, this is a script will prompt you to search something on YouTube and the play it in mpv media player.

The youtube_dmenu.sh script will run the search.py script in order to search the YouTube so you need to have them in the same directory for this to work.  

The mediaNotification script prints the volumeLevel and the audio metadata in a dunst notification using the playerctl program also will change the media. If the correct flag has been used. 

* p for player-pause toggle
* n for the next song in a playlist 
* b for previus
* t for stop 
* s for status 

I use this script in my i3wm config file to control the media from multimedia keys in my keyboard.

# Power script 

power.sh is one script that lounge a dmenu script with the options:

* poweroff 
* reboot
* logout

The logout command work only on i3wm. Does not work on hyprland. 

I use this on i3wm config to control my system from a keybind (ctrl + p)

# Screenshots 

screenshots.sh is one script to take screenshots and the save them or add them to the clip board. Using dmenu and the program maim. The script ask you where you want to save it and what you want to print

* full screen 
* selected area 
* focus window 

# Sites 

The sites.sh script just prompt on dmenu some sites i use and when selected opens firefox on that site.

# volumeMeter 

Volume meter is a script that sends a dunst notification to show the volume number in % When volume is changing i use this on i3wm config as well.

# wallpaper 
This is just a line to change the random wallpaper with feh. I use cronjob to run this is command every 15 min. You can also use the ``SelecteWallpeper.sh``to select and set wallpapers on your system. 

# Mount
This is script to mount and umound devices via dmenu

# updateInScript
This is a script that installs and updates my scripts on a system
curent tested os LinuxMint
Working on archlinux, Ubuntu(untested).

# mediaNotifications
This is a script to display media relate notifications like the audio volume and the media playing/

# notificationCenter
This is a script that manage the custom notifications icon in the bar and in the feture will control the notificaton program (dunst at the moment) 

# Tip 
On most of my scripts theres is a line that defines the location of the script in order to work properly. You need to change this line to your location of the script. Most of the times the variable is the $locationScript or something like that.

# scriptUpdater 
This script installs or updates (re install) all the scripts of mine. 
Also installes or (re installs) all the dependenceis. 

This script works on:

* ArchLinux (only at the moment) 

On some distros some staff may be missing. I also working to add support 
on elementary os and Fedora. 

# Backup
This a script to backup my files to other devices for backup

This scripts works only on archlinux at the moment


# Disclamer 

All this scripts is work in progress. 
I try to make them work but i can not guarantee that will work. 
If you do not know how to check the scripts by your self, please do not run them. 
Also snapshots is recomended before running my scripts. 

