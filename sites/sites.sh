#!/bin/bash

mode=("open" "openall" "save" "delete")
modeForNewFiles=("YES" "NO")
modeForDeleting=("list" "bookmark")
defaultPlaceBookmark=$HOME/.config/scripts/bookmarks
browserProfiles=("FN" "FM" "FMP" "FP" "FS")
checks()
{
    bookmarkDir=$(ls $defaultPlaceBookmark)
    if [ -z "$bookmarkDir" ]
    then
	mkdir "$defaultPlaceBookmark"
    fi
}

selectMode()
{
    type=$(printf "%s\n" "${mode[@]}" | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 4 -p "What you want to do ?: ")
}

findTheLink()
{
    whatFileToOpen=$(ls "$defaultPlaceBookmark" | grep links -v | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 10 -p "Choose a bookmark list")
    whatToOpen=$(cat "$defaultPlaceBookmark"/"$whatFileToOpen" | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 10 -p "Where you want to go ?: ")
    noExtensionFile=${whatFileToOpen%.*}
    echo "This is the noExtensionFile $noExtensionFile"
    searchForLinkHere=$(ls "$defaultPlaceBookmark" | grep "$noExtensionFile" | grep links)
    linkLine=$(grep -n "$whatToOpen" "$defaultPlaceBookmark/$whatFileToOpen" | cut -d: -f1)
    echo "This is the searchForLinkHere $defaultPlaceBookmark/$searchForLinkHere"
    echo "This is the linkLine $linkLine"
    whatLinkToOpen=$(awk -v z="$linkLine" 'NR==z' "$defaultPlaceBookmark"/"$searchForLinkHere")
}

openLink()
{
    if [ -z "$whatLinkToOpen" ]
    then
	dunstify "Bookmarks" "Nothing has selected"
    else
	firefox "$whatLinkToOpen" -P "$profile" --name "$name"
    fi
}

takeSaveInfo()
{
    if [ "$newFile" == "NO" ]
    then
	whatFileToSave=$(ls "$defaultPlaceBookmark" | grep links -v | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 10 -p "Choose a bookmark list")
    fi
    noExtensionFile=${whatFileToSave%.*}
    echo "THis is the no extensionfile $noExtensionFile"
    whatFileLinksToSave=$(ls "$defaultPlaceBookmark" | grep "$noExtensionFile" | grep links) linkToSave=$(rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -p "Paste or type the link you want to save : ")
    echo "This is the $whatFileLinksToSave"
    titleToSave=$(rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -p "Give a title to the bookmark: ")
}

saveBookmark()
{
    if [ "$newFile" == "NO" ]
    then
	echo "This is the placeToSace $placeToSave"
	placeToSave="$defaultPlaceBookmark/$whatFileToSave"
	placeToSaveLink="$defaultPlaceBookmark/$whatFileLinksToSave"
    elif [ "$newFile" == "YES" ]
    then
	placeToSave="$newPlace"
	placeToSaveLink="$newPlaceLink"
    fi
    
    if [ -z "$linkToSave" ] && [ "$titleToSave" ]
    then
	dunstify "Bookmarks" "Nothing to save"
	exit 1
    else
	echo "$titleToSave" >> "$placeToSave" 
	echo "$linkToSave" >> "$placeToSaveLink" 
	dunstify "bookmarks" "The new bookmark has been saved"
    fi
}

deleteBookmark()
{
    whatTypeToDelete=$(printf "%s\n" "${modeForDeleting[@]}" |rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -p "Do you want to delete a bookamark or bookmark list ? " -l 2)
    if [ "$whatTypeToDelete" == "list" ]
    then
	whatToDelete=$(ls "$defaultPlaceBookmark" | grep links -v | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 10 -p "Choose a bookmark list to delete")
	noExtensionFile=$(echo "$whatToDelete" | head -c4)
	whatListToDelete=$(ls "$defaultPlaceBookmark" | grep "$noExtensionFile" | grep links)
	rm "$defaultPlaceBookmark"/"$whatToDelete"
	rm "$defaultPlaceBookmark"/"$whatListToDelete"
    elif [ "$whatTypeToDelete" == "bookmark" ]
    then
	fromWhatFileToDelete=$(ls "$defaultPlaceBookmark" | grep links -v | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 10 -p "Choose a bookmark to delete")
	fromWhatFileToDeleteLinks=$(ls "$defaultPlaceBookmark/$fromWhatFileToDelete" | grep links)
	whatToDelete=$(cat "$defaultPlaceBookmark/$fromWhatFileToDelete" | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -p "What do you want to delete: " -l 10)
	lineToDelete=$(grep -n "$whatToDelete" "$defaultPlaceBookmark/$fromWhatFileToDelete" | cut -d: -f1)
	sed -i "$lineToDelete"d "$defaultPlaceBookmark/$fromWhatFileToDelete"
	sed -i "$lineToDelete"d "$defaultPlaceBookmark/$fromWhatFileToDeleteLinks"
	dunstify "bookmarks" "Bookmark deleted"
    fi

    if [ -z "$whatToDelete" ]
    then
	dunstify "Bookmarks" "Nothing to delete"
    fi
}


createNewFiles()
{
    nameOfNewFiles=$(rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 0 -p "Name the new folder")
    touch "$defaultPlaceBookmark"/"$nameOfNewFiles".txt
    touch "$defaultPlaceBookmark"/"$nameOfNewFiles"links.txt
    newPlace=$HOME/.config/scripts/bookmarks/"$nameOfNewFiles".txt
    newPlaceLink=$HOME/.config/scripts/bookmarks/"$nameOfNewFiles"links.txt
}

checkIfNewFileNeeded()
{
    newFile=$(printf "%s\n" "${modeForNewFiles[@]}" | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 2 -p "Do you want new subbookmark")
}

openAll()
{
    whatFileToOpen=$(ls "$defaultPlaceBookmark" | grep links -v | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 10 -p "Choose a bookmark list")
    echo "$whatFileToOpen"
    noExtensionFile=${whatFileToOpen%.*}
    linkFile=$(ls "$defaultPlaceBookmark" | grep "$noExtensionFile" | grep links)
    echo "$linkFile"
    lineNumber=$(cat "$defaultPlaceBookmark/$linkFile" | wc -l )
    for i in "${lineNumber[@]}" 
    do
	link=$(head -n $i "$defaultPlaceBookmark/$linkFile")
	firefox -P "$profile" --name "$name" $link
    done
}

selectBrowserProfile()
{
    browserProfile=$(printf "%s\n" "${browserProfiles[@]}"  | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 5 -p "Choose Browser profile :")

    if [ "$browserProfile" == "FN" ]
    then
	name="FN"
	profile="FN"
    elif [ "$browserProfile" == "FM" ]
    then
	name="FM"
	profile="FM"
    elif [ "$browserProfile" == "FMP" ]
    then
	name="FAMP"
	profile="FMP"
    elif [ "$browserProfile" == "FP" ]
    then
	name="FP"
	profile="FP"
    elif [ "$browserProfile" == "FS" ]
    then
	name="FS"
	profile="FS"
    fi
}

selectMode
if [ "$type" == "open" ]
then
    selectBrowserProfile
    findTheLink
    openLink
    exit 0
elif [ "$type" == "openall" ]
then
    selectBrowserProfile
    openAll
    exit 0 
elif [ "$type" == "save" ]
then
    checkIfNewFileNeeded
    if [ "$newFile" == "YES" ]
    then
	createNewFiles
    fi
    takeSaveInfo
    saveBookmark
    exit 0 
elif [ "$type" == "delete" ]
then 
    deleteBookmark
else
    dunstify "BookMarks" "Nothing is selected"
    exit 0
fi
exit 0
