# This is the Dependencies for my scripts (Wayland only). 

* kitty (terminal emulator)
* ranger (Cli filemanager)
* nomacs (image viewer)
* rofi-wayland(searching tool)
* hyprshot (printscreen tool)
* dunst (notification deamon)
* hyprland (wayland compositor)
* radeontop (AMD gpu stats)
* swaybg (set the wallpaper)
* mpv-mpris (mpv with the plug in to d-bus support) (play multimedia files)
* xdotool
* zenity
* git (manage git repos)
* rsync (sync files)
* jq
* lsb-release (Display the version of the O.S.)
* hyprlock (Screen Locker)
