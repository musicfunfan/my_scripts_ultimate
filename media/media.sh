#!/bin/bash

places=("video" "YouTube" "music" "photos" "media_url" "media_urls_list" "save to playlist" "exit_menu")
options=("play all" "play all shuffle" "play one" "exit_menu")
path=$(printf "%s\n" "${places[@]}"| rofi -dmenu -theme /home/$USER/.config/rofi/second_theme/dmenu_edited.rasi -i -l 8 -p 'select media: ' )
locationOfScript="$HOME/.config/scripts"

playlistMode()
{
    if [ "$1" == "shuffle" ]
    then
        mpv "$2" --"$1" --save-position-on-quit
    elif [ "$1" == "in_order" ]
    then
        mpv "$2" --save-position-on-quit
    fi
}

if [ "$path" == "video" ]
then 
    mode=$(printf "%s\n" "${options[@]}" | rofi -dmenu -theme /home/$USER/.config/rofi/second_theme/dmenu_edited.rasi -p "Select Mode" -l 4)

    if [ "$mode" == "play all" ]
    then
	kitty ranger --choosedir=/tmp/rangerChoose /media/musicfan/hdd/video 
	locationPath=$(cat /tmp/rangerChoose)
        playlistMode "in_order" "$locationPath"
    elif [ "$mode" == "play all shuffle" ]
    then
	kitty ranger --choosedir=/tmp/rangerChoose /media/musicfan/hdd/video
	locationPath=$(cat /tmp/rangerChoose)
        playlistMode "shuffle" "$locationPath"
    elif [ "$mode" == "play one" ]
    then
	kitty ranger --choosefiles=/tmp/rangerChoose /media/musicfan/hdd/video
	playThis=$(cat /tmp/rangerChoose)
	echo "This is the playThis $playThis"
        mpv "$playThis"

    else
        dunstify "Media Script" "Nothing is selected"
        exit 0
    fi
fi
    
if [ "$path" == "music" ]
then
    choice=$(printf "%s\n" "${options[@]}"|rofi -dmenu -theme /home/$USER/.config/rofi/second_theme/dmenu_edited.rasi -i -l 20 -p 'select media: ' )
    
    if [ "$choice" == "play all" ]
    then    
	kitty ranger --choosedir=/tmp/rangerChoose  /media/musicfan/hdd/music
	locationPath=$(cat /tmp/rangerChoose)
        mpv "$locationPath" 
    fi  
    
    if [ "$choice" == "play all shuffle" ]
    then    
	kitty ranger --choosedir=/tmp/rangerChoose /media/musicfan/hdd/music 
	locationPath=$(cat /tmp/rangerChoose)
        mpv "$locationPath" --shuffle
    fi

    if [ "$choice" == "play one" ]
    then
	kitty ranger --choosefiles=/tmp/rangerChoose /media/musicfan/hdd/music 
	locationPath=$(cat /tmp/rangerChoose)
	mpv "$locationPath"
    fi
    
    if [ "$choice" == "exit_menu" ]
    then
        exit 1
    fi 
fi

if [ "$path" == "photos" ]
then
    choice=$(printf "%s\n" "${options[@]}"|rofi -dmenu -theme /home/$USER/.config/rofi/second_theme/dmenu_edited.rasi -i -l 20 -p 'select media: ' )
    
    if [ "$choice" == "play all" ]
    then    
	kitty ranger --choosedir=/tmp/rangerChoose  /media/musicfan/hdd/pictures
	locationPath=$(cat /tmp/rangerChoose)
        nomacs "$locationPath"  
    fi  
    
    if [ "$choice" == "play one" ]
    then
	kitty ranger --choosefiles=/tmp/rangerChoose /media/musicfan/hdd/pictures 
	locationPath=$(cat /tmp/rangerChoose)
	nomacs "$locationPath"
    fi
fi


if [ "$choice" == "exit_menu" ]
then
    exit 1
fi

if [ "$path" == "YouTube" ]
then 
    /home/musicfan/.config/scripts/youtube_dmenu.sh
    exit 1
fi

if [ "$path" == "media_url" ]
then
    locationPath=$(rofi -dmenu -i -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -p 'paste-here the url:')

    bash "$locationOfScript/saveToPlaylist.sh" "$locationPath"
    
    
    if [ "$locationPath" != "''" ]
    then
        mpv "$locationPath" --save-position-on-quit 
    fi
    
    exit 1
fi

if [ "$path" == 'media_urls_list' ]
then
    locationPath=$(rofi -show file-browser-extended -file-browser-dir $HOME -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -file-browser-stdout)
    
 
    printf "%s\n" "${options[@]}" > $locationOfScript/bookmarkList.txt
    python3 "$locationOfScript/readLinks.py" >> $locationOfScript/bookmarkList.txt "$locationPath"
    choice=$(cat $locationOfScript/bookmarkList.txt |rofi -dmenu -i -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -l 20 -p "Choose link")
    
    if [ "$choice" == "play all" ]
    then
        mpv --playlist="$locationOfScript/bookmarkList.txt" --no-resume-playback
    fi

    if [ "$choice" == "play all shuffle" ]
    then
        mpv --playlist="$locationOfScript/bookmarkList.txt" --shuffle --save-position-on-quit --no-resume-playback
    fi

    if [ "$choice" == "exit_menu" ]
    then
        rm $locationOfScript/bookmarkList.txt
        exit 1
    fi

    if [ "$choice" != "play all" ] && [ "$choice" != "play all shuffle" ] && [ "$choice" != "exit_menu" ]
    then
        mpv "$choice" --save-position-on-quit --no-resume-playback
    fi
    rm $locationOfScript/bookmarkList.txt
    exit 1
fi

if [ "$path" == 'save to playlist' ]
then 
    locationPath=$(rofi -dmenu -i -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -p 'What you want to save:' -l 0)
    "$locationOfScript/saveToPlaylist.sh" "$locationPath"
    dunstify -a "Media" "Media script" "Saved ➡ $locationPath"
fi

dunstify -a "Media" "Media script" "stop playing"


exit 1
