import urllib
import re
import sys
import os
from yt_dlp import YoutubeDL
ydl = YoutubeDL({'quiet': True})
ydl.add_default_info_extractors 

listREsults=[]
# get the info from the user
inputArgs = sys.argv
search = inputArgs[1]

# read the html of the page and fined the urls
html = urllib.request.urlopen("https://www.youtube.com/results?search_query=" + search.replace(" ", "+"))
video_ids = re.findall(r"watch\?v=(\S{11})", html.read().decode())

# check if the input is integer
a=10
i = os.environ.get('TIMERUN')
urls = []

# loop to fill the urls array to pass the urls to the yt-dlp
for x in range(int(a)):
    i = int (i) + 1
    urls.append("https://www.youtube.com/watch?v=" + video_ids[i])
    
# input the urls in ydl1(youtube-d
for link in urls:
    with ydl:
        result = ydl.extract_info(link, download=False,)
        title = result['title']
        listREsults.append(title)
z=0
for titles in listREsults:
    print (titles)
    print (urls[z])
    z=z+1


