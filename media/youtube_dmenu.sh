#!/bin/bash

#This is a script that lets you navigate from rofi and listen or watch
#videos from mpv and yt-dlp and rofi 

#This is a method to save the basic locations in the memory
initialized ()
{
   locationScript="$HOME/.config/scripts/customPlaylists" 
    
    modes=(
            "choose from history"
            "something else"
            "change media playing quality"
            "change media playing quality of the playlist"
            "play all in order"
            "play all in random order"
            "Create Custom Playlist"
            "Load old play list"
)
    
    choiceVideo=$(printf "%s\n" "${modes[@]}" | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 8 : -p 'YouTube')
    mode=$(echo "$choiceVideo" | awk '{print $1}')
    export TIMERUN=$(( -1 ))
    if [ "$mode" != "saved" ] && [ "$choiceVideo" != "something else"  ] && [ "$mode" != "play" ] && [ "$mode" != "play all in random order" ] && [ "$mode" != "something" ] && [ "$choiceVideo" != "change media playing quality" ] && [ "$choiceVideo" != "change media playing quality of the playlist" ] && [ "$choiceVideo" != "Create Custom Playlist" ] && [ "$choiceVideo" != "choose from history" ] && [ "$choiceVideo" != "Load old play list" ] 
    then
        yad --text-info --text="You can choiceVideo for somthing using somthing else 
        option \n or you can select a option from the histroy (if you a histroy) \n try that"
        exit 1
    fi
}

#This is a method to load all the links in file
loadAllTheLinks ()
{
    playList=$(cat  $locationScript/historyUrls.txt | cut -d ' ' -f2-)
    echo "$playList" >> "$locationScript"/playList.txt
    echo "This is the playlist $playList"
}

#This is a function to get the links form YouTube
searchYt ()
{
    echo "Start searching"
    
    if [ "$1" != more ]
    then
        searchActual=$(rofi -dmenu -i -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -p 'search on YouTube : ')
    fi
    
    if [ -z "$searchActual"  ]
    then
        exit 1 
    fi
    
    searchResults=$(python3 /home/$USER/.config/scripts/searchYt.py "$searchActual")
    export TIMERUN=$(( TIMERUN + 10 ))
}

#This is the method to load the data from the history files
loadFiles ()
{
    #Load the files
    historyTitleList=$(less "$locationScript/historyTitle.txt")
    historyUrlsList=$(less "$locationScript/historyUrls.txt")
}

#Find all the links from the history
findUrlsFromHistory ()
{
    #Count the rows of the historyTitle file
    countRowsTitleFile=$(less "$locationScript"/historyTitle.txt | wc -l) 

    #Count the rows of the historyUrls file
    countRowsUrlsFile=$(less "$locationScript"/historyUrls.txt | wc -l)

    #Fint all the Titles from history
    for ((t=0; t<="$countRowsTitleFile"; t++))
    do 
        listToArray=$(echo "$historyTitleList" | awk "FNR == $t")
       resultTitlesArray+=("$listToArray")
    done    
    

    #Fint all the Urls from history
    for ((u=0; u<="$countRowsUrlsFile"; u++))
    do 
        listToArray=$(echo "$historyUrlsList" | awk "FNR == $u")
       resultLinksArray+=("$listToArray")
    done
    
}

#This function is to find all the urls for the titles
searchForUrlsNet ()
{   
    resultLinksArray=()
    resultTitlesArray=() 
    array=()
    
    #Find all titles
    for a in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
    do  
        listToArray=$(echo "$searchResults" | awk "FNR ==$a")
        array+=("$listToArray")
    done

    for a in {0..20..2}
    do 
        resultsTitles=$(echo -e "${array[$a]}")
        resultTitlesArray+=("$resultsTitles")
    done
    resultTitlesArray+=(more)

    #Find all the urls
    for l in {1..21..2}
    do 
        resultLinks=$(echo -e "${array[$l]}")
        resultLinksArray+=("$resultLinks")
    done
    
}

#This is to print all the availible results to the dmenu to prompt the user
resultDmenu ()
{   


    if [ "$1" == "historyMode" ]
    then
        choiceVideo=$(less "$locationScript/historyTitle.txt" | rofi -i -dmenu -l 30 -theme "$HOME/.config/rofi/second_theme/dmenu_edited.rasi")
    fi

    
    test2=$(less "$locationScript"/historyTitle.txt )
    createPlaylistOptions=("Playlist Completed" "$test2")
    if [ "$1" == "customPlaylist" ]
    then
        while [ "$choiceVideo" != "Playlist Completed" ]
        do 
            choiceVideo=$( printf "%s\n" "${createPlaylistOptions[@]}"   | dmenu -i -l 25 -p 'YouTube')
            
            if [ "$choiceVideo" != "Playlist Completed" ]
            then

                playlistArray+=("$choiceVideo")
            fi
        done
    fi
    
    if [ "$1" == "listResult" ] 
    then
       choiceVideo=$(printf "%s\n" "${resultTitlesArray[@]}" | rofi -dmenu  -theme "$HOME/.config/rofi/second_theme/dmenu_edited.rasi" -i -l 10  -p "Choose a result")
       
    fi
    
    if [ "$1" == "listQualityVideo" ] 
    then
	echo "---------------------"
	printf "%s\n" "${arrayQuality[@]}"
	echo "---------------------"
        videoQuality=$(printf "%s\n" "${arrayQuality[@]}"  | rofi -dmenu -theme "$HOME/.config/rofi/second_theme/dmenu_edited.rasi"  -i -l 10 -p "Choose a video quality")
    fi
    
    if [ "$1" == "listQualityAudio" ] 
    then
        audioQuality=$(printf "%s\n" "${arrayQuality[@]}"  |rofi -dmenu -theme "$HOME/.config/rofi/second_theme/dmenu_edited.rasi"  -i -l 10 -p "Choose a audio quality")
    fi
    
}

#Link all urls to all Titles for the new stuff
linksUrls ()
{    
    
    for i in "${!resultTitlesArray[@]}";
    do
        if [[ "${resultTitlesArray[$i]}" = "${choiceVideo}" ]];
        then
            index=$i
            break
        fi
    done 
   
    if [ "$index" -gt -1 ]
    then
        savedToPlayOne=$(echo "${resultLinksArray[$index]}" | cut -d" " -f2)
    else
        echo "$choiceVideo is not in Array."    
    fi
    
}

#This is a mehtod to save new searches in the disk
saveNewSearches ()
{
    if [ -n "$choiceVideo" ]
    then
        echo "saved $choiceVideo" >> "$locationScript/historyTitle.txt"
        echo "saved ${resultLinksArray[$index]}" >> "$locationScript/historyUrls.txt"
    fi

    #Removing doubles from the history files
    awk '!x[$0]++' $locationScript/historyTitle.txt >> ./historyTitle2.txt
    awk '!x[$0]++' $locationScript/historyUrls.txt >> ./historyUrls2.txt
    mv $locationScript/historyTitle2.txt $locationScript/historyTitle.txt
    mv $locationScript/historyUrls2.txt $locationScript/historyUrls.txt 

}   

#This method finds all the quality options for the curent link
findQualityOptions ()
{
    if [ "$choiceVideo" == "change media playing quality" ] 
    then
        mediaToChangeQuality=$savedToPlay
    fi

    if [ "$mode" == "choose" ] || [ "$mode" == "something" ]
    then
        mediaToChangeQuality=$(echo "${resultLinksArray[$index]}" | cut -d" " -f2)
    fi
   
    qualityList=$(yt-dlp "$mediaToChangeQuality" -Fq)
    lineCount=$(echo "$qualityList" | wc -l)
    
    for ((c=0; c<="$lineCount"; c++))
    do 
        listToArray=$(echo "$qualityList" | awk "FNR == $c")
        arrayQuality+=("$listToArray")
    done

echo "this is $savedToPlay"
}

#Get the codes for the quality choice 
getTheQualityCodes ()
{
    choiceVideoQuality=$(echo "$videoQuality" | awk '{printf $1}')
    choiceAudioQuality=$(echo "$audioQuality" | awk '{printf $1}')
    
    if [ -z "$choiceAudioQuality"  ] || [ -z "$choiceVideoQuality" ]
    then 
        choiceAudioQuality="best"
        choiceVideoQuality="best"
    fi
}    

#play the video that loaded with mpv media player via mpv
playVideo ()
{
    if [ "$1" == "oldPlaylist" ]
    then
        mpv --playlist="$locationScript/$oldPlaylistChoice" 
    fi
    
    if [ "$1" == "customPlaylist" ]
    then
        mpv --ytdl-format="$choiceVideoQuality"+"$choiceAudioQuality" --playlist="$locationScript/playList.txt" --save-position-on-quit --no-resume-playback 
    fi
    
    if [ "$1" == "resume" ]
    then 
        mpv --ytdl-format="$choiceVideoQuality"+"$choiceAudioQuality" "$savedToPlay" --save-position-on-quit
    fi

    if [ "$1" == "resumePlayList" ]
    then 
        mpv --ytdl-format="$choiceVideoQuality"+"$choiceAudioQuality" --playlist="$locationScript/playList.txt" --save-position-on-quit
    fi
    #
    if [ "$1" != "playLIstInOrder" ] && [ "$1" != "playLIstInRundomOrder" ] && [ "$1" != "resume" ] && [ "$1" != "customPlaylist" ] && [ "$1" != "oldPlaylist" ] 
        then
        mpv --ytdl-format="$choiceVideoQuality"+"$choiceAudioQuality" "$savedToPlayOne" --save-position-on-quit --no-resume-playback 
    fi
    
    if [ "$1" == "playLIstInOrder" ]
    then
        mpv --playlist=$locationScript/playList.txt --save-position-on-quit --no-resume-playback 
    fi

    if [ "$1" == "playLIstInRundomOrder" ]
    then
        mpv --playlist=$locationScript/playList.txt --shuffle --save-position-on-quit --no-resume-playback 
    fi  
}

#deleting old files
deleteOldFiles ()
{
    truncate -s 0 $locationScript/playList.txt
    truncate -s 0 $locationScript/playList.txt
}

#Saves the file the media to resume playback
saveNowPlay ()
{
    if [ "$1" != "playlistMode" ]
    then
        savedToPlay=$(playerctl metadata | grep url | awk '{printf $3}')
    fi

    if [ "$1" == "playlistMode" ]
    then
        savedToPlay=$(cat $locationScript/playList.txt)
        mediaToChangeQuality=$(playerctl metadata | grep url | awk '{printf $3}')
    fi
}

#delete the old watch_later from the mpv
deletePreviuseSavedMpvState ()
{
    rm -r "$HOME"/.config/mpv/watch_later
}

#Create the new custom playlist
createNewCustomPlaylist ()
{
    echo "This is in new custom playlist function"
    echo "creating"
    echo "${resultLinksArray[$index]}" | cut -d" " -f2 >> "$locationScript"/playList.txt
    echo "This is the the link to save"
    echo "done creating"
}

#Save the custom playlist
saveCustomPlaylist ()
{
    yesNo=("Yes" "No")
    savePlaylist=$(printf "%s\n" "${yesNo[@]}" | rofi -dmenu -l 2 -theme "$HOME/.config/rofi/second_theme/dmenu_edited.rasi" -p "Do you want to save the playlist :")
    
    if [ "$savePlaylist" == "Yes" ]
    then
        mkdir "$locationScript/customPlaylists"
        nameOfPlaylist=$(echo "exit_menu" | rofi -dmenu -i -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -p "Give a name for the playlist :")
        if [ "$nameOfPlaylist" == exit_menu ]
        then
            exit 1
        else
            cp "$locationScript"/playList.txt "$locationScript/customPlaylists/$nameOfPlaylist.txt"
        fi
    else
        dunstify -a "Media" "YouTube script" "Playlist will not be saved"
    fi
}


loadOldPlaylist ()
{
    echo "This is the loadOldPlaylist function"
    oldPlaylistChoice=$(ls $HOME/.config/scripts/customPlaylists | rofi -dmenu -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -i -l 10 -p "Choose a playlist to load: ")
}    


saveToGenerelaPlaylist ()
{
    bash "$locationScript/saveToPlaylist.sh" "$1"
}

#I need to work on that. 
selectMediaFromPlaylist ()
{
    selectMedia=(rofi -show file-browser-extended -q -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -file-browser-stdout -file-browser-dir /home/musicfan/.config/scripts/customPlaylist)
}

#Calling functions from above
initialized
if [ "$mode" != "saved" ] && [ "$choiceVideo" == "something else" ]
then
    searchYt
    searchForUrlsNet
    resultDmenu listResult
    
    while [ "$choiceVideo" == "more" ]
    do
        searchYt more
        searchForUrlsNet
        resultDmenu listResult
    done    
    saveToGenerelaPlaylist "${resultLinksArray[$index]}"
    linksUrls net
    saveNewSearches
    findQualityOptions 
    resultDmenu listQualityVideo
    resultDmenu listQualityAudio
    getTheQualityCodes
    playVideo net
fi

#What to do when user loads links form the history
if [ "$choiceVideo" == "choose from history" ]
then
    resultDmenu historyMode
    deleteOldFiles
    loadFiles
    findUrlsFromHistory
    linksUrls 
    findQualityOptions 
    resultDmenu listQualityVideo
    resultDmenu listQualityAudio
    getTheQualityCodes
    playVideo 
fi

#What to do if user selects play all in order 
if [ "$mode" != "saved" ] && [ "$choiceVideo" == "play all in order" ]
then
    deleteOldFiles
    loadAllTheLinks
    getTheQualityCodes
    playVideo playLIstInOrder 
fi

#What to do if user selects play all in order 
if [ "$mode" != "saved" ] && [ "$choiceVideo" == "play all in random order" ]
then
    deleteOldFiles
    loadAllTheLinks
    getTheQualityCodes
    playVideo playLIstInRundomOrder 
fi

#This what to do when user wants to change the quality on the fly
if [ "$choiceVideo" == "change media playing quality of the playlist" ]
then
    deletePreviuseSavedMpvState
    saveNowPlay playlistMode
    sleep 4
    killall mpv
    findQualityOptions
    resultDmenu listQualityVideo
    resultDmenu listQualityAudio
    getTheQualityCodes
    playVideo resumePlayList
    deleteOldFiles
fi

#This what to do when user wants to change the quality on the fly
if [ "$choiceVideo" == "change media playing quality" ]
then
    deletePreviuseSavedMpvState
    saveNowPlay 
    sleep 4
    killall mpv
    findQualityOptions
    resultDmenu listQualityVideo
    resultDmenu listQualityAudio
    getTheQualityCodes
    playVideo resume 
    deleteOldFiles
fi

#This is the what to do when user want to create custom playlist
if [ "$choiceVideo" == "Create Custom Playlist" ]
then
    deleteOldFiles
    loadFiles
    findUrlsFromHistory
    resultDmenu customPlaylist
    printf "%s\n" "${playlistArray[@]}"
    
    for items in "${playlistArray[@]}"
    do
        choiceVideo="$items"
        linksUrls
        createNewCustomPlaylist
    done
    getTheQualityCodes
    saveCustomPlaylist
    playVideo customPlaylist
    
fi

# This is to play old playlist
if [ "$choiceVideo" == "Load old play list" ]
then
    deletePreviuseSavedMpvState
    loadOldPlaylist
    selectMediaFromPlaylist
    playVideo oldPlaylist 
fi
exit 1
