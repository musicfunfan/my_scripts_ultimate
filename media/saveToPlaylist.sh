#!/bin/bash

#This is a script to save playing media to a playlist.


locationOfScript="/home/musicfan/.config/scripts"
yesNoArray=("Yes" "No")
oldPlaylist=$(ls $locationOfScript/customPlaylists)
playListToSaveArray=("$oldPlaylist" "Create New Playlist")

addLinkToPlaylist ()
{
    yesNo=$(printf "%s\n" "${yesNoArray[@]}" | rofi -dmenu -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -l 2 -p "Do you want to add this to a playlist")
    if [[ "$yesNo" == "No" ]]
    then 
        dunstify -a "Media" "Media secript" "This will not be saved to playlist"
    elif [[ "$yesNo" == "Yes" ]]
    then
        playlistSaved=$(printf "%s\n" "${playListToSaveArray[@]}" | rofi -dmenu -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -l 20 -p "Choose playlist to add")
    else
        dunstify -a "Media" "Media.sh" "The script has exited"
        exit 1
    fi
 
    if [[ "$playlistSaved" == "Create New Playlist" ]]
    then
        nameOfPlaylist=$(rofi -dmenu -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -p "Give a name to the new playlist")
        mkdir "$locationOfScript/customPlaylists"
        echo "$1" >> "$locationOfScript/customPlaylists/$nameOfPlaylist"
    elif [[ "$playlistSaved" != "Create New Playlist" ]]
    then
        echo "$1" >> "$locationOfScript/customPlaylists/$playlistSaved"
        mkdir "$locationOfScript/customPlaylists"
        awk '!x[$0]++' "$locationOfScript"/customPlaylists/"$playlistSaved" >> "$locationOfScript"/customPlaylists/tempSave
        mv "$locationOfScript"/customPlaylists/tempSave "$locationOfScript"/customPlaylists/"$playlistSaved"
   fi
 
 }  

addLinkToPlaylist "$1"
