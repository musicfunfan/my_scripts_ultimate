#!/usr/bin/bash

#This is a script to save notification history and also put a notification counter on the bar (polybar)
# Wait for new notification to happen and then save them on a file.

#find the location of this script
findLocation ()
{
    scriptLocation=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
    echo "$scriptLocation" > /home/$USER/.config/scripts/locationFileNot.txt
}

waitForNotifications ()
{
    lineCountStart=$(cat $scriptLocation/notificationsLogs.txt | wc -l)
    echo "This is the lineCountStart $lineCountStart"
    dbus-monitor "interface='org.freedesktop.Notifications'" |\
    grep --line-buffered "string" |\
    grep --line-buffered -e method -e ":" -e '""' -e urgency -e notify -v |\
    grep --line-buffered '.*(?=string)|(?<=string).*' -oPi |\
    grep --line-buffered -v '^\s*$' >> $scriptLocation/notificationsLogs.txt &
    status="normal"
}
# Wait until the file ./notificationsLogs.txt gets new data.
# out put the number of lines.
# This while is an ifinity loop.
lineCount ()
{
    while [ "$status" == "normal" ]
    do
        inotifywait $scriptLocation/notificationsLogs.txt --event=modify -o $scriptLocation/events.txt
        lineCount=$(cat $scriptLocation/events.txt | wc -l)
        lineChang="yes"
        date >> $scriptLocation/notificationsLogs.txt
    done
}

clearLogShown ()
{
    truncate -s 0 $scriptLocation/events.txt
    truncate -s 0 $scriptLocation/notificationsLogs.txt
}


#This is to show if the notification is muted or not.
disableNotifications ()
{
    location=$(cat /home/$USER/.config/scripts/locationFileNot.txt)
    cd $location
    dunstStatus=$(dunstctl is-paused)
    lineCountStart=$(cat ./events.txt | wc -l)

    if [ "$dunstStatus" == true ]
    then
        echo "🔕 $lineCountStart"
    else
        echo "🔔 $lineCountStart"
    fi
}

while getopts 'nsha:' OPTION; do
  case "$OPTION" in
    n)
        findLocation
        clearLogShown
        waitForNotifications
        lineCount
        ;;
    s)
        disableNotifications
        ;;
    ?)
      echo "script usage: $(basename \$0) [-l] [-h] [-a somevalue]" >&2
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"
