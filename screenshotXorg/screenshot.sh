#!/bin/bash

setUp()
{
    rofiTheme="$HOME/.config/rofi/second_theme/dmenu_edited.rasi"
    scriptLocation="$HOME/.config/scripts"
    choiceOptions=("target" "all" "more")
    choiceSaveOptions=("clip" "home" "custom" "exit")
    homeLocation="$HOME/screenshots"
}

screenshotSave()
{
    choiceSave=$(printf "%s\n" ${choiceSaveOptions[@]} | rofi -theme "$rofiTheme" -dmenu -i -l 4 -p 'Where to save ?: ')
}

screenshotMode()
{
    choice=$(printf "%s\n" ${choiceOptions[@]} | rofi -theme "$rofiTheme" -dmenu -i -l 3 -p 'Type: ' )
}

saveClip()
{
    if [ "$choice" == "target" ]
    then
        maim --delay=1 -i "$(xdotool getactivewindow)" | xclip -selection clipboard -t image/png
    fi 

    if [ "$choice" == "all" ]
    then
       maim --delay=1 | xclip -selection clipboard -t image/png 
    fi
    
    if [ "$choice" == "more" ]
    then
    	maim -s --delay=1 | xclip -selection clipboard -t image/png
    fi
}

saveHome()
{
     if [ ! -d "$homeLocation" ]
     then
	 echo "This is for new direcrory"
	 mkdir "$homeLocation"
     fi
    
    if [ "$choice" == "target" ]
    then
	echo "This is for target"
        maim --delay=1 -i "$(xdotool getactivewindow)" "$homeLocation/screenshot.png"
    fi 
    
    if [ "$choice" == "all" ]
    then
       maim --delay=1 "$homeLocation/screenshot.png"
    fi
    
    if [ "$choice" == "more" ]
    then
    	maim -s --delay=1 "$homeLocation/screenshot.png"
    fi
}

saveCustom()
{
    echo "This is the bad code"
    choiceLocation=$(zenity --file-selection --directory)
    choiceName=$(rofi -theme "$rofiTheme" -dmenu -i -l 20 -p 'give a name for the file: ')
    
    if [ "$choiceName" == "use the default name" ]
    then
        choiceName="screenshot"
    fi
    
    if [ "$choice" == "target" ] && [ "$choiceName" != "exit_menu" ]
    then
        maim --delay=1 -i "$(xdotool getactivewindow)" "$choiceLocation"/"$choiceName".png
    fi 
    
    if [ "$choice" == "all" ] && [ "$choiceName" != "exit_menu" ]
    then
       maim --delay=1 "$choiceLocation"/"$choiceName".png
    fi
    
    if [ "$choice" == "more" ] && [ "$choiceName" != "exit_menu" ]
    then
    	maim -s --delay=1 "$choiceLocation"/"$choiceName".png
    fi
}

exitScript()
{
    exit 0 
}

setUp
screenshotSave

if [ "$choiceSave" == "exit" ]
then
    exit 1
fi    

screenshotMode

if [ "$choiceSave" == "home" ]
then
    echo "This is the home"
    saveHome
elif [ "$choiceSave" == "custom" ]
then
    saveCustom
elif [ "$choiceSave" == "clip" ]
then
   saveClip 
fi
