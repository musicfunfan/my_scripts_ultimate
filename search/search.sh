browserProfiles=("FN" "FM" "FMP" "FP" "FS")

getSearchEngine()
{
    options=("Google" "DuckDuckGo" "Brave" "Bing" "YouTube" "Twitch" "Odysse" "NetFlix" "ArchWiki" "Custom Adres")
    searchEngine=$(printf "%s\n" "${options[@]}" | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -l 10 -i -p "Choose a search engine")
}
getSearchTerm()
{
    searchTerm=$(rofi -dmenu -theme /home/musicfan/.config/rofi/second_theme/dmenu_edited.rasi -l 0 -p "Search with $searchEngine")
}

googleSearch()
{
    firefox -P "$profile" "https://www.google.com/search?q=$searchTerm" --name "$name"
}

youtubeSearch()
{
    firefox -P "$profile" "https://www.youtube.com/results?search_query=$searchTerm" --name "$name"
}

duckduckgoSearch()
{
    firefox -P "$profile" "https://duckduckgo.com/?q=$searchTerm" --name "$name" 
}

braveSearch()
{
    firefox -P "$profile" "https://search.brave.com/search?q=$searchTerm" --name "$name"
}

bingSearch()
{
    firefox -P "$profile" "https://www.bing.com/search?q=$searchTerm" --name "$name"
}

netflixSearch()
{
    firefox -P "$profile" "https://www.netflix.com/search?q=$searchTerm" --name "$name"
}

twichSearch()
{
    firefox -P "$profile" "https://www.twitch.tv/search?term=$searchTerm" --name "$name"
}

odysseSearch()
{
    firefox -P "$profile" "https://odysee.com/$/search?q=$searchTerm" --name "$name"
}

archwikiSearch()
{
    firefox -P "$profile" "https://wiki.archlinux.org/index.php?search=$searchTerm" --name "$name"
}

adresSearch()
{
    firefox -P "$profile" "$searchTerm" 
}

selectBrowserProfile()
{
    profile=$(printf "%s\n" "${browserProfiles[@]}"  | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 5 -p "Choose Browser profile :")

    if [ "$profile" == "FN" ]
    then
	name="FN"
	profile="FN"
    elif [ "$profile" == "FM" ]
    then
	name="FM"
	profile="FM"
    elif [ "$profile" == "FMP" ]
    then
	name="FAMP"
	profile="FMP"
    elif [ "$profile" == "FP" ]
    then
	name="FP"
	profile="FP"
    elif [ "$profile" == "FS" ]
    then
	name="FS"
	profile="FS"
    fi
}

getSearchEngine
selectBrowserProfile
if [ "$searchEngine" == "Google" ]
then
    getSearchTerm
    googleSearch

elif [ "$searchEngine" == "DuckDuckGo" ]
then
    getSearchTerm
    duckduckgoSearch

elif [ "$searchEngine" == "Brave" ]
then
    getSearchTerm
    braveSearch

elif [ "$searchEngine" == "Bing" ]
then
    getSearchTerm
    bingSearch

elif [ "$searchEngine" == "YouTube" ]
then
    getSearchTerm
    youtubeSearch

elif [ "$searchEngine" == "NetFlix" ]
then
    getSearchTerm
    netflixSearch

elif [ "$searchEngine" == "Twitch" ]
then
    getSearchTerm
    twichSearch

elif [ "$searchEngine" == "Odysse" ]
then
    getSearchTerm
    odysseSearch

elif [ "$searchEngine" == "ArchWiki" ]
then
    getSearchTerm
    archwikiSearch
elif [ "$searchEngine" == "Custom Adres" ]
then
    getSearchTerm
    adresSearch
fi

