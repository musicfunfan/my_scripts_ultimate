profileOptions=("FN" "FP" "FS" "FM" "FMP")

selectProfile ()
{
    profile=$(printf "%s\n" "${profileOptions[@]}" | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 5 -p "Select a profile: ")
}

classWindow ()
{
    if [ "$profile" = "FN" ]
    then
	name="FN"
    elif [ "$profile" = "FP" ]
    then
	name="FP"
    elif [ "$profile" = "FS" ]
    then
	name="FS"
    elif [ "$profile" = "FM" ]
    then
	name="FM"
    elif [ "$profile" = "FMP" ]
    then
	name="FAMP"
    fi
    
	
}

selectProfile
classWindow
firefox -P "$profile" --name "$name"
