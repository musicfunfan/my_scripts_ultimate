#!/bin/bash

save_options=("home" "custom location" "clip")
screenshot_types=("target window" "region" "all screens" "exit script")

timeToDelay=$(echo "Type the value in second" | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 20 -p 'How much time to you want: ')
scriptLocation="$HOME/.config/scripts"
saveOptions=$(printf "%s\n" "${save_options[@]}" | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -l 3 -p "Choose what do to with the screenshot")
choice=$(printf "%s\n" "${screenshot_types[@]}" | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 4 -p 'Type: ' )

#If the type is only the focus window do this:
if [ "$choice" == "target window" ]
then
    sleep "$timeToDelay"

    if [ "$saveOptions" == "clip" ]
    then
        hyprshot -m window --clipboard-only
    fi

    if [ "$saveOptions" == "home" ]
    then
        choiseName=$(rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 1 -p 'Give a name for the screenshot' )
	hyprshot -m window -o "$HOME/screenshots" -f "$choiseName"
    fi

    if [ "$saveOptions" == "custom location" ]
    then
        choiseName=$(rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 1 -p 'Give a name for the screenshot')
        choiseSave=$(rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 1 -p 'Where you want to save the screenshot')
        hyprshot -m window -f "$choiseName" -o "$choiseSave"
    fi
fi

#If the type is only the focus full screen/s do this:
if [ "$choice" == "all screens" ]
then
    sleep "$timeToDelay"

    if [ "$saveOptions" == "clip" ]
    then
        hyprshot -m output --clipboard-only
    fi

    if [ "$saveOptions" == "home" ]
    then
        choiseName=$(rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 1 -p 'Give a name for the screenshot' )
        hyprshot -m output -f "$choiceName" -o "$HOME"
    fi

    if [ "$saveOptions" == "custom location" ]
    then
        choiseName=$(rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 1 -p 'Give a name for the screenshot')
        choiseSave=$(rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 1 -p 'Where you want to save the screenshot')
        hyprshot -m output -f "$choiceName" -o "$choiceSave"
    fi
fi

#If the type is only the focus full screen/s do this:
if [ "$choice" == "region" ]
then
    sleep "$timeToDelay"

    if [ "$saveOptions" == "clip" ]
    then
        hyprshot -m region --clipboard-only
    fi

    if [ "$saveOptions" == "home" ]
    then
        choiseName=$(rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 1 -p 'Give a name for the screenshot' )
        hyprshot -m region -f "$choiceName" -o "$HOME"
    fi

    if [ "$saveOptions" == "custom location" ]
    then
        choiseName=$(rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 1 -p 'Give a name for the screenshot')
        choiseSave=$(rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l 1 -p 'Where you want to save the screenshot')
        hyprshot -m region -f "$choiceName" -o "$choiceSave"
    fi
fi

if [ "$choice" == "exit script" ]
then
    exit 0
fi
