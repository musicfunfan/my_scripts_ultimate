#!/bin/bash

#This is a script to change and set wallpapers.

historyFile="$HOME/.config/scripts/wallPaperHistoryLocation.txt"
curentWallpaper="$HOME/.config/scripts/curentWallpaper.txt"
wallpaperSettings="$HOME/.config/scripts/wallpeperHistory.txt"
whatToKill="$HOME/.config/scripts/wallpaperScriptID.txt"

checks()
{
    whatToKillCheck=$(ls "$whatToKill")
    historyFileCheck=$(ls "$historyFile")
    curentWallpaperCheck=$(ls "$curentWallpaper")
    wallpaperSettingsCheck=$(ls "$wallpaperSettings")

    if [ -z "$historyFileCheck" ] || [ -z "$curentWallpaperCheck" ] || [ -z  "$wallpaperSettingsCheck" ] || [ -z "$whatToKillCheck" ]
    then
	dustify "Wallpaper Script" "Files Create at $HOME/.config/scripts"
	touch "$historyFile"
	touch "$curentWallpaper"
	touch "$wallpaperSettings"
	touch "$whatToKill"
    fi
}

selectMode ()
{
    options=("stable" "changing" "exit")
    mode=$(printf "%s\n" "${options[@]}" | rofi -show -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -dmenu -i -l 6 -p "Select Mode ")
    echo "$mode" >> "$wallpaperSettings"
}

loadWallPaperLocation ()
{
    truncate -s 0 $HOME/.config/scripts/wallpeperHistory.txt
    location=$(printf "%s\n" "${historyPath[@]}" | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -i -l "$lineCount" -p "Load wallpaper path: ")
    echo "$location" >> "$wallpaperSettings"
}

saveHistory()
{
    exist=$(cat "$historyFile" | grep "$location")
    if [ -z "$exist" ]
    then
	echo "$location" >> "$historyFile"
    fi
}

loadOldHistory()
{
    mapfile -t historyPath < "$historyFile"
    lineCount=$(cat "$historyFile" | wc -l)
}

#This is a method to find $location of the files in mode chaging
changingDirectory ()
{
    options=("random" "in_order")
    modeChanging=$(printf "%s\n" "${options[@]}" | rofi -dmenu -theme "$HOME"/.config/rofi/second_theme/dmenu_edited.rasi -i -l 2 -p "choose mode for changing" )
    echo "$modeChanging" >> "$wallpaperSettings"
}

#This is a method to find the $location of the files in mode srable
stableWallpeper ()
{
    wall=$(ls "$location" | rofi -dmenu -theme $HOME/.config/rofi/second_theme/dmenu_edited.rasi -l 10 i -p "Select a wallpaper")
}

#This is to exit the script
exitScript ()
{
    exit 0
}

#This is a method to set the wallpaper
setWallpeperStable ()
{
    swaybg -m fit -i "$location/$wall" &
    echo "$location/$wall" > "$curentWallpaper"
}

#This is a method to set the wallpaper
setWallpeperChanging ()
{
    if [ "$1" == "random" ]
    then
       wallpaper=$(ls "$location" |sort -R | awk 'FNR==1')
       swaybg -m fit -i "$location/$wallpaper" &
       echo "$location/$wallpaper" > "$curentWallpaper"
    fi

    if [ "$1" == "in_order" ]
    then
       wallpaper=$(ls "$location" | awk 'FNR==1')
       swaybg -m fit -i "$location/$wallpaper" &
       echo "$location/$wallpaper" > "$curentWallpaper"
    fi
}

infinityLoop()
{
    echo "This is the infinityLoop is enabled"
    while :
    do
    if [ "$1" == "random" ]
    then
       killallSwaybg
       wallpaper=$(ls "$location" |sort -R | awk 'FNR==1')
       swaybg -m fit -i "$location/$wallpaper" &
       echo "$location/$wallpaper" > "$curentWallpaper"
    fi

    if [ "$1" == "in_order" ]
    then
       killallSwaybg
       wallpaper=$(ls "$location" | awk 'FNR==1')
       swaybg -m fit -i "$location/$wallpaper" &
       echo "$location/$wallpaper" > "$curentWallpaper"
    fi
	sleep 1200
    done
}

#This is a method to kill all swaybg procces
killallSwaybg()
{
    killall swaybg &
}

#Save the pid of the script
savePID()
{
    pgrep -f SelectWallpeper.sh > "$whatToKill"
}

#THis is to kill previus instansis of the script.
killallScripts()
{
    kill "$pid" &
}

#Load old script PID in a variable
oldPID()
{
    pid=$(cat "$whatToKill")
}

checks
oldPID
savePID
killallSwaybg

if [ "$1" == "old" ]
then
   mode=$(cat "$wallpaperSettings" | awk 'NR==2 {print; exit}')
   location=$(cat "$wallpaperSettings" | awk 'NR==1 {print; exit}')
   modeChanging=$(cat "$wallpaperSettings" |awk 'NR==3 {print; exit}')

   if [ "$mode" == "changing" ]
   then
       if [ "$modeChanging" == "random" ]
       then
       setWallpeperChanging "random"
       infinityLoop "random"
       else
           setWallpeperChanging "in_order"
           infinityLoop "in_order"
        fi
        exit 0
    fi

    if [ "$mode" == "stable"  ]
    then
        stableWallpeper
        setWallpeperStable
        exit 0
    fi

    if [ "$mode" == "exit" ]
    then
    exit 0
    fi
fi

if [ -z "$1" ]
then
    killallScripts
    loadOldHistory
    loadWallPaperLocation
    saveHistory
    selectMode
fi

if [ "$mode" == "changing" ] && [ -z "$1" ]
then
    changingDirectory
    if [ "$modeChanging" == "random" ]
    then
    setWallpeperChanging "random"
    infinityLoop "random"
    else
        setWallpeperChanging "in_order"
        infinityLoop "in_order"
    fi
    exit 0
fi

if [ "$mode" == "stable" ] && [ -z "$1" ]
then
    stableWallpeper
    setWallpeperStable
    exit 0
fi

if [ "$mode" == "exit" ] && [ -z "$1" ]
then
   exit 0
fi
